const express = require('express')
const router = express.Router()
const authMiddleware = require('../middlewares/authMiddleware')
const { postTruck, assignTruckById, getTrucks, getTruckById, updateTruckById, deleteTruckById } = require('../controllers/trucksController')

router.get('/trucks', authMiddleware, getTrucks)
router.post('/trucks', authMiddleware, postTruck)
router.get('/trucks/:id', authMiddleware, getTruckById)
router.put('/trucks/:id', authMiddleware, updateTruckById)
router.delete('/trucks/:id', authMiddleware, deleteTruckById)
router.post('/trucks/:id/assign', authMiddleware, assignTruckById)

module.exports = router
