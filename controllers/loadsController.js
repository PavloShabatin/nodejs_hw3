const jwt = require('jsonwebtoken')
const Truck = require('../models/truck')
const Load = require('../models/load')
const {SPRINTER, SMALL_STRAIGHT, LARGE_STRAIGHT} = require('../models/trucks')
const {getId, getRole} = require('../scripts/helpers')

module.exports.postLoad = (request, response) => {
    const {name, payload, pickup_address, delivery_address, dimensions} = request.body
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'SHIPPER') {
            getId(email).then((created_by) => {
                const load = new Load({created_by, name, payload, pickup_address, delivery_address, dimensions})
                load.save()
                    .then(() => {
                        if (!created_by || !name || !payload || !pickup_address || !delivery_address || !dimensions) {
                            return response.status(400).json({message: 'string'})
                        }
                        return response.json({message: 'Success'})
                    })
                    .catch(() => {
                        return response.status(500).json({message: 'string'})
                    })
            })
        } else {
            return response.status(500).json({message: 'string'})
        }
    })
}

module.exports.getLoads = (request, response) => {
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'SHIPPER') {
            getId(email).then((created_by) => {
                Load.find({created_by}, {__v: 0})
                    .then(loads => {
                        if (!loads) {
                            return response.status(400).json({message: 'string'})
                        }
                        return response.json({loads})
                    })
                    .catch(() => {
                        return response.status(500).json({message: 'string'})
                    })
            })
        } else if (role === 'DRIVER') {
            getId(email).then((assigned_to) => {
                Load.find({assigned_to}, {__v: 0})
                    .then(loads => {
                        if (!loads) {
                            return response.status(400).json({message: 'string'})
                        }
                        return response.json({loads})
                    })
                    .catch(() => {
                        return response.status(500).json({message: 'string'})
                    })
            })
        }
    })
}

module.exports.getActiveLoad = (request, response) => {
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'DRIVER') {
            getId(email).then((assigned_to) => {
                Load.findOne({assigned_to, status: 'ASSIGNED'}, {__v: 0})
                    .then(loads => {
                        if (!loads) {
                            return response.status(400).json({message: 'string'})
                        }
                        return response.json({loads})
                    })
                    .catch(() => {
                        return response.status(500).json({message: 'string'})
                    })
            })
        } else {
            return response.status(500).json({message: 'string'})
        }
    })
}

module.exports.iterateToNextLoadState = (request, response) => {
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    const states = Load.schema.paths.state.enumValues
    getRole(email).then((role) => {
        if (role === 'DRIVER') {
            getId(email).then((assigned_to) => {
                Load.findOne({assigned_to}, {})
                    .then(load => {
                        if (!load || states.indexOf(load.state) === states.length - 1) {
                            return response.status(400).json({message: 'string'})
                        }
                        Load.findOneAndUpdate({assigned_to}, {state: states[states.indexOf(load.state) + 1]}, {new: true})
                            .then(iterLoad => {
                                if (iterLoad.state === 'Arrived to delivery') {
                                    iterLoad.status = 'SHIPPED'
                                    iterLoad.logs.push({message: `Load state changed to '${iterLoad.state}'`, time: Date.now()})
                                    iterLoad.logs.push({message: `Load status changed to 'SHIPPED'`, time: Date.now()})
                                    iterLoad.save()
                                    Truck.findOneAndUpdate({assigned_to}, {status: 'OS'})
                                        .then(truck => {
                                            if (!truck) {
                                                return response.status(400).json({message: 'string'})
                                            }
                                            return response.json({message: `Load state changed to '${iterLoad.state}'`})
                                        })
                                        .catch(() => {
                                            return response.status(500).json({message: 'string'})
                                        })
                                }
                                iterLoad.logs.push({message: `Load state changed to '${iterLoad.state}'`, time: Date.now()})
                                iterLoad.save()
                                return response.json({message: `Load state changed to '${iterLoad.state}'`})
                            })
                            .catch(() => {
                                return response.status(500).json({message: 'string'})
                            })
                    })
                    .catch(() => {
                        return response.status(500).json({message: 'string'})
                    })
            })
        } else {
            return response.status(500).json({message: 'string'})
        }
    })
}

module.exports.getLoadById = (request, response) => {
    const id = request.params.id
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'SHIPPER') {
            getId(email).then((created_by) => {
                Load.find({_id: id, created_by}, {__v: 0})
                    .then(load => {
                        if (!load) {
                            return response.status(400).json({message: 'string'})
                        }
                        return response.json({load})
                    })
                    .catch(() => {
                        return response.status(500).json({message: 'string'})
                    })
            })
        }
    })
}

module.exports.updateLoadById = (request, response) => {
    const id = request.params.id
    const {name, payload, pickup_address, delivery_address, dimensions} = request.body
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'SHIPPER') {
            Load.findById(id)
                .then(load => {
                    if (!load || load.status !== 'NEW') {
                        return response.status(400).json({message: 'string'})
                    }
                    Load.findByIdAndUpdate({_id: id}, {
                        name,
                        payload,
                        pickup_address,
                        delivery_address,
                        dimensions
                    }, {new: true})
                        .then(load => {
                            if (!load) {
                                return response.status(400).json({message: 'string'})
                            }
                            load.logs.push({message: `Load was updated by fields: ${name}, ${payload}, ${pickup_address}, ${delivery_address}, ${dimensions}`})
                            load.save()
                            return response.json({message: "Success"})
                        })
                        .catch(() => {
                            return response.status(500).json({message: 'string'})
                        })
                })
                .catch(() => {
                    return response.status(500).json({message: 'string'})
                })
        } else {
            return response.status(500).json({message: 'string'})
        }
    })
}

module.exports.deleteLoadById = (request, response) => {
    const id = request.params.id
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'SHIPPER') {
            Load.findById(id)
                .then(load => {
                    if (!load || load.status !== 'NEW') {
                        return response.status(400).json({message: 'string'})
                    }
                    Load.findByIdAndRemove({_id: id})
                        .then(load => {
                            if (!load || load.deletedCount === 0) {
                                return response.status(400).json({message: 'string'})
                            }
                            return response.json({message: 'Success'})
                        })
                        .catch(() => {
                            return response.status(500).json({message: 'string'})
                        })
                })
                .catch(() => {
                    return response.status(500).json({message: 'string'})
                })
        } else {
            return response.status(500).json({message: 'string'})
        }
    })
}

module.exports.postLoadById = (request, response) => {
    const id = request.params.id
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'SHIPPER') {
            Truck.find({status: 'OS'})
                .then(trucks => {
                    trucks = trucks.filter(t => t.assigned_to !== null)
                    Load.findById(id)
                        .then(load => {
                            if (!load || load.status !== 'NEW') {
                                return response.status(400).json({message: 'string'})
                            }
                            Load.findByIdAndUpdate(id, {status: 'POSTED'})
                                .then(load => {
                                    const sprinters = trucks.filter(t => t.type === 'SPRINTER')
                                    const smStraights = trucks.filter(t => t.type === 'SMALL STRAIGHT')
                                    const lgStraights = trucks.filter(t => t.type === 'LARGE STRAIGHT')
                                    if (sprinters.length > 0 && load.dimensions.length <= SPRINTER.length && load.dimensions.width <= SPRINTER.width
                                        && load.dimensions.height <= SPRINTER.height && load.payload <= SPRINTER.payload) {
                                        for (let i = 0; i < trucks.length; i++) {
                                            if (trucks[i].type === 'SPRINTER') {
                                                trucks[i].status = 'IS'
                                                trucks[i].save()
                                                load.assigned_to = trucks[i].assigned_to
                                                load.status = 'ASSIGNED'
                                                load.logs.push({message: `Load was assigned to driver ${load.assigned_to}`})
                                                load.save()
                                                return response.json({message: 'Success'})
                                            }
                                        }
                                    } else if (smStraights.length > 0 && load.dimensions.length <= SMALL_STRAIGHT.length && load.dimensions.width <= SMALL_STRAIGHT.width
                                        && load.dimensions.height <= SMALL_STRAIGHT.height && load.payload <= SMALL_STRAIGHT.payload) {
                                        for (let i = 0; i < trucks.length; i++) {
                                            if (trucks[i].type === 'SMALL STRAIGHT') {
                                                trucks[i].status = 'IS'
                                                trucks[i].save()
                                                load.assigned_to = trucks[i].assigned_to
                                                load.status = 'ASSIGNED'
                                                load.logs.push({message: `Load was assigned to driver ${load.assigned_to}`})
                                                load.save()
                                                return response.json({message: 'Success'})
                                            }
                                        }
                                    } else if (lgStraights.length > 0 && load.dimensions.length <= LARGE_STRAIGHT.length && load.dimensions.width <= LARGE_STRAIGHT.width
                                        && load.dimensions.height <= LARGE_STRAIGHT.height && load.payload <= LARGE_STRAIGHT.payload) {
                                        for (let i = 0; i < trucks.length; i++) {
                                            if (trucks[i].type === 'LARGE STRAIGHT') {
                                                trucks[i].status = 'IS'
                                                trucks[i].save()
                                                load.assigned_to = trucks[i].assigned_to
                                                load.status = 'ASSIGNED'
                                                load.logs.push({message: `Load was assigned to driver ${load.assigned_to}`})
                                                load.save()
                                                return response.json({message: 'Success'})
                                            }
                                        }
                                    }
                                    load.status = 'NEW'
                                    load.save()
                                    return response.status(400).json({message: 'string'})
                                })
                                .catch(() => {
                                    return response.status(500).json({message: 'string'})
                                })
                        })
                        .catch(() => {
                            return response.status(500).json({message: 'string'})
                        })
                })
                .catch(() => {
                    return response.status(500).json({message: 'string'})
                })
        } else {
            return response.status(500).json({message: 'string'})
        }
    })
}

module.exports.getLoadShippingInfoById = (request, response) => {
    const id = request.params.id
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'SHIPPER') {
            getId(email).then((created_by) => {
                Load.find({_id: id, created_by}, {__v: 0})
                    .then(load => {
                        if (!load || load.state !== 'ASSIGNED') {
                            return response.status(400).json({message: 'string'})
                        }
                        return response.json({load})
                    })
                    .catch(() => {
                        return response.status(500).json({message: 'string'})
                    })
            })
        }
    })
}





