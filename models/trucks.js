module.exports.SPRINTER = {
    width: 300,
    length: 250,
    height: 170,
    payload: 1700
}

module.exports.SMALL_STRAIGHT = {
    width: 500,
    length: 250,
    height: 170,
    payload: 2500
}


module.exports.LARGE_STRAIGHT = {
    width: 700,
    length: 350,
    height: 200,
    payload: 4000
}

