const mongoose = require('mongoose')
const Schema = mongoose.Schema

module.exports = mongoose.model('registrationCredential', new Schema({
    email: {
        required: true,
        type: String,
        unique: true
    },
    password: {
        required: true,
        type: String
    },
    role: {
        required: true,
        enum: ['SHIPPER', 'DRIVER'],
        type: String,
        default: 'SHIPPER'
    }
}))
