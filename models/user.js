const mongoose = require('mongoose')
const Schema = mongoose.Schema

module.exports = mongoose.model('user', new Schema({
    email: {
        required: true,
        type: String,
        unique: true
    },
    created_date: {
        required: true,
        type: Date,
        default: Date.now()
    },
    role: {
        required: true,
        type: String,
        enum: ['SHIPPER', 'DRIVER'],
        default: ['DRIVER']
    }
}))
