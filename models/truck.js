const mongoose = require('mongoose')
const Schema = mongoose.Schema

module.exports = mongoose.model('truck', new Schema({
    created_by: {
        required: true,
        type: String
    },
    assigned_to: {
        type: String,
        default: null
    },
    type: {
        required: true,
        type: String,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
        default: 'SPRINTER'
    },
    status: {
        required: true,
        type: String,
        enum: ['OS', 'IS'],
        default: 'OS'
    },
    created_date: {
        required: true,
        type: Date,
        default: Date.now()
    }
}))
