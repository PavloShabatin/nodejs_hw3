const jwt = require('jsonwebtoken')
const User = require('../models/user')
const Credential = require('../models/credentials')

module.exports.getUser = (request, response) => {
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    User.findOne({email}, {__v: 0}).exec()
        .then(user => {
            if (!user) {
                return response.status(400).json({message: 'string'})
            }
            return response.json({user})
        })
        .catch(() => {
            return response.status(500).json({message: 'string'})
        })
}

module.exports.deleteUser = (request, response) => {
    const [, token] = request.headers.authorization.split(' ');
    const email = jwt.decode(token).email
    User.remove({email}).exec()
        .then(user => {
            if (user.deletedCount === 0) {
                return response.status(400).json({message: 'string'})
            }
            return response.json({message: "Success"})
        })
        .catch(() => {
            return response.status(500).json({message: 'string'})
        })
    Credential.remove({email}).exec()
        .then(user => {
            if (user.deletedCount === 0) {
                return response.status(400).json({message: 'string'})
            }
            return response.json({message: "Success"})
        })
        .catch(() => {
            return response.status(500).json({message: 'string'})
        })
}

module.exports.changePassword = (request, response) => {
    const {oldPassword, newPassword} = request.body
    const update = {password: newPassword}
    const [, token] = request.headers.authorization.split(' ');
    const email = jwt.decode(token).email
    Credential.findOneAndUpdate({email}, update).exec()
        .then(user => {
            if(!user || oldPassword !== user.password){
                return response.status(400).json({message: 'string'})
            }
            return response.json({message: "Success"})
        })
        .catch(() => {
            return response.status(500).json({message: 'string'})
        })
}
