const express = require('express')
const router = express.Router()
const authMiddleware = require('../middlewares/authMiddleware')
const { getLoads, postLoad, getActiveLoad, iterateToNextLoadState, getLoadById, updateLoadById, deleteLoadById, postLoadById, getLoadShippingInfoById } = require('../controllers/loadsController')

router.get('/loads', authMiddleware, getLoads)
router.post('/loads', authMiddleware, postLoad)
router.get('/loads/active', authMiddleware, getActiveLoad)
router.patch('/loads/active/state', authMiddleware, iterateToNextLoadState)
router.get('/loads/:id', authMiddleware, getLoadById)
router.put('/loads/:id', authMiddleware, updateLoadById)
router.delete('/loads/:id', authMiddleware, deleteLoadById)
router.post('/loads/:id/post', authMiddleware, postLoadById)
router.get('/loads/:id/shipping_info', authMiddleware, getLoadShippingInfoById)

module.exports = router
