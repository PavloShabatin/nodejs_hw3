const jwt = require('jsonwebtoken')
const User = require('../models/user')
const Credential = require('../models/credentials')
const RegistrationCredential = require('../models/registrationCredentials')
const {secret} = require('../config/auth')

module.exports.register = (request, response) => {
    const {email, password, role} = request.body
    const user = new User({email, createdDate: new Date(), role})
    const credential = new Credential({email, password})
    const registrationCredential = new RegistrationCredential({email, password, role})
    registrationCredential.save()
        .then(() => {
            if (!email || !password || !role) {
                return response.status(400).json({message: 'string'})
            }
            credential.save()
                .then(() => {
                    user.save()
                        .then((user) => {
                            console.log(user._id)//5fb96622594be72844bfb150
                            return response.json({message: 'Success'})
                        })
                        .catch(() => {
                            return response.status(500).json({message: 'string'})
                        })
                })
                .catch(() => {
                    return response.status(500).json({message: 'string'})
                })
        })
        .catch(() => {
            return response.status(500).json({message: 'string'})
        })
}

module.exports.login = (request, response) => {
    const {email, password} = request.body

    Credential.findOne({email, password}).exec()
        .then(user => {
            if (!user) {
                return response.status(400).json({message: 'No user with such username or password found'})
            }
            return response.json({jwt_token: jwt.sign(JSON.stringify(user), secret)})
        })
        .catch(() => {
            return response.status(500).json({message: 'string'})
        })
}

module.exports.forgotPassword = (request, response) => {
    const {email} = request.body

    Credential.findOne({email}).exec()
        .then(user => {
            if (!user) {
                return response.status(400).json({message: 'string'})
            }
            return response.json({message: 'New password sent to your email address'})
        })
        .catch(() => {
            return response.status(500).json({message: 'string'})
        })
}
