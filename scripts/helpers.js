const RegistrationCredential = require('../models/registrationCredentials')
const User = require('../models/user')

module.exports.getRole = (email) => {
    return new Promise((res) => {
        RegistrationCredential.findOne({email}).exec()
            .then(user => {
                res(user.role)
            })
    })
}

module.exports.getId = (email) => {
    return new Promise((res) => {
        User.findOne({email})
            .then(user => {
                res(user._id)
            })
    })
}
