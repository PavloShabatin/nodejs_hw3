const jwt = require('jsonwebtoken')
const Truck = require('../models/truck')
const {SPRINTER, SMALL_STRAIGHT, LARGE_STRAIGHT} = require('../models/trucks')
const {getId, getRole} = require('../scripts/helpers')

const types = [SPRINTER, SMALL_STRAIGHT, LARGE_STRAIGHT]

module.exports.postTruck = (request, response) => {
    const {type} = request.body
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'DRIVER') {
            getId(email).then((created_by) => {
                const truck = new Truck({created_by, type})
                truck.save()
                    .then(() => {
                        if (!created_by && !types.includes(type)) {
                            return response.status(400).json({message: 'string'})
                        }
                        return response.json({message: 'Success'})
                    })
                    .catch(() => {
                        return response.status(500).json({message: 'string'})
                    })
            })
        } else {
            return response.status(500).json({message: 'string'})
        }
    })
}

module.exports.assignTruckById = (request, response) => {
    const id = request.params.id
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'DRIVER') {
            getId(email).then((assigned_to) => {
                Truck.findOne({assigned_to})
                    .then(truck => {
                        if(!truck){}
                        else if (truck.status === 'IS') {
                            return response.status(400).json({message: 'string'})
                        }
                        else if (truck.status === 'OS') {
                            truck.assigned_to = null
                            truck.save()
                        }
                        Truck.findOneAndUpdate({_id: id}, {$set: {assigned_to: assigned_to}}, {new: true})
                            .then(truck => {
                                if (!truck) {
                                    return response.status(400).json({message: 'string'})
                                }
                                return response.json({message: 'Truck assigned successfully'})
                            })
                            .catch(() => {
                                return response.status(500).json({message: 'string'})
                            })
                    })
            })
        } else {
            return response.status(500).json({message: 'string'})
        }
    })
}

module.exports.getTrucks = (request, response) => {
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'DRIVER') {
            getId(email).then((created_by) => {
                Truck.find({created_by}, {__v: 0})
                    .then(trucks => {
                        if (!trucks) {
                            return response.status(400).json({message: 'string'})
                        }
                        return response.json({trucks})
                    })
                    .catch(() => {
                        return response.status(500).json({message: 'string'})
                    })
            })
        } else {
            return response.status(500).json({message: 'string'})
        }
    })
}

module.exports.getTruckById = (request, response) => {
    const id = request.params.id
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'DRIVER') {
            getId(email).then((created_by) => {
                Truck.findOne({_id: id, created_by}, {__v: 0})
                    .then(truck => {
                        if (!truck) {
                            return response.status(400).json({message: 'string'})
                        }
                        return response.json({truck})
                    })
                    .catch(() => {
                        return response.status(500).json({message: 'string'})
                    })
            })
        } else {
            return response.status(500).json({message: 'string'})
        }
    })
}

module.exports.updateTruckById = (request, response) => {
    const {type} = request.body
    const id = request.params.id
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'DRIVER') {
            getId(email).then((created_by) => {
                Truck.findOne({assigned_to: created_by})
                    .then(assignedTruck => {
                        if (assignedTruck._id.toString() === id) {
                            return response.status(400).json({message: 'string'})
                        } else {
                            Truck.findOneAndUpdate({_id: id, created_by}, {$set: {type: type}}, {new: true})
                                .then(truck => {
                                    if (!truck) {
                                        return response.status(400).json({message: 'string'})
                                    }
                                    return response.json({message: 'Truck details changed successfully'})
                                })
                                .catch(() => {
                                    return response.status(500).json({message: 'string'})
                                })
                        }
                    })
                    .catch(() => {
                        return response.status(500).json({message: 'string'})
                    })
            })
        } else {
            return response.status(500).json({message: 'string'})
        }
    })
}

module.exports.deleteTruckById = (request, response) => {
    const id = request.params.id
    const [, token] = request.headers.authorization.split(' ')
    const email = jwt.decode(token).email
    getRole(email).then((role) => {
        if (role === 'DRIVER') {
            getId(email).then((created_by) => {
                Truck.findOne({assigned_to: created_by})
                    .then(assignedTruck => {
                        if (assignedTruck._id.toString() === id) {
                            return response.status(400).json({message: 'string'})
                        } else {
                            Truck.findByIdAndRemove(id)
                                .then(truck => {
                                    if (truck.deletedCount === 0) {
                                        return response.status(400).json({message: 'string'})
                                    } else {
                                        return response.json({message: 'Truck deleted successfully'})
                                    }
                                })
                                .catch(() => {
                                    return response.status(500).json({message: 'string'})
                                })
                        }
                    })
                    .catch(() => {
                        return response.status(500).json({message: 'string'})
                    })
            })
        } else {
            return response.status(500).json({message: 'string'})
        }
    })
}
