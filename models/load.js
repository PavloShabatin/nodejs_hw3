const mongoose = require('mongoose')
const Schema = mongoose.Schema

module.exports = mongoose.model('load', new Schema({
    created_by: {
        required: true,
        type: String
    },
    assigned_to: {
        type: String,
        default: null
    },
    status: {
        required: true,
        type: String,
        enum: ['NEW', 'POSTED', 'ASSIGNED', 'SHIPPED'],
        default: 'NEW'
    },
    state: {
        required: true,
        type: String,
        enum: ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'],
        default: 'En route to Pick Up'
    },
    name: {
        required: true,
        type: String
    },
    payload: {
        required: true,
        type: Number
    },
    pickup_address: {
        required: true,
        type: String
    },
    delivery_address: {
        required: true,
        type: String
    },
    dimensions: {
        required: true,
        type: Object,
        width: {
            required: true,
            type: Number
        },
        length: {
            required: true,
            type: Number
        },
        height: {
            required: true,
            type: Number
        }
    },
    logs: {
        required: true,
        type: [{
            message: {
                required: true, type: String
            },
            time: {
                required: true, type: Date, default: Date.now()
            }
        }],
        default: []
    },
    created_date: {
        required: true,
        type: Date,
        default: Date.now()
    }
}))
